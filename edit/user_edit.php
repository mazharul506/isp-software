<?php
$userid = isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;

$token = isset($_GET['token'])? $_GET['token'] :NULL;

$data = $obj->details_by_cond('vw_user_info',"UserId='$token'");
      extract($data);

?>

        <div class="col-md-12" style=" background-image:url(asset/img/content_h1.png); margin-top:20px; margin-bottom: 15px; min-height:40px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">
            <b>Edit Form</b>
        </div>

        <form action="?q=view_user" enctype="multipart/form-data" method="POST">    
        <div class="row" style="padding:10px; font-size: 12px;">
          
            <div class="col-md-6">

                  <input type="hidden" name="user_id" value="<?php echo isset($token)? $token :NULL; ?>" />

                  <input type="hidden" name="ph_path" value="<?php echo isset($data['PhotoPath'])? $data['PhotoPath'] :NULL; ?>" />
                  <div class="form-group">
                    <label for="exampleInputEmail1">Full Name</label>
                    <input type="text" name="full_name" value="<?php echo isset($data['FullName'])? $data['FullName'] :NULL; ?>" class="form-control" id="exampleInputEmail1" placeholder="Full Name" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" name="user_name" value="<?php echo isset($data['UserName'])? $data['UserName'] :NULL; ?>" class="form-control" id="exampleInputEmail1" placeholder="User Name" required>
                  </div>
<!--                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="**********" required>
                  </div>-->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email Address</label>
                    <input type="email" name="email" value="<?php echo isset($data['Email'])? $data['Email']:NULL; ?>" class="form-control" id="exampleInputEmail1" placeholder="Email Address" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mobile No</label>
                    <input type="text" name="mobile_no" value="<?php echo isset($data['MobileNo'])? $data['MobileNo'] :NULL; ?>" class="form-control" id="exampleInputEmail1" placeholder="Mobile No" required>
                  </div>
                
                  <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <input type="text" name="address" value="<?php echo isset($data['Address'])? $data['Address'] :NULL; ?>" class="form-control" id="exampleInputEmail1" placeholder="Address" required>
                  </div>
            </div>

            <div class="col-md-2"></div>
            <div class="col-md-2"></div>

            <div class="col-md-4">
            <?php if($data['PhotoPath']=='0') { ?>
              <div class="form-group">
                <img width="140" height="140" src="asset/img/def_img.png" alt="..." class="img-thumbnail" id="pre_photo">
              </div>
            <?php
              } 
              else{
            ?>

              <div class="form-group">
                <img width="140" height="140" src="<?php echo isset($data['PhotoPath'])? $data['PhotoPath'] :NULL; ?>" alt="..." class="img-thumbnail" id="pre_photo">
              </div>
              <?php } ?>
            
            <div class="form-group">
                <label for="exampleInputFile">Chose Photo</label>
                <input type="file" name="user_photo" onchange="usershow_photo(this)" id="photo">
            </div>        

              <div class="form-group">
                <label for="exampleInputEmail1">Office Location</label>
                  <select name="national_id" class="form-control" style="margin-bottom: 5px;" required>
                    <option <?php if ($data['NationalId']=='1') echo 'selected'; ?> value="1">Head Office</option>
                    <option <?php if ($data['NationalId']=='2') echo 'selected'; ?> value="2">Branch Office</option>
                  </select>
              </div>     
              <div class="form-group">
                <label for="exampleInputEmail1">User Type</label>
                  <select name="user_type" class="form-control" style="margin-bottom: 5px;" required>
                  <option value="">- - - - -</option>
                    <option <?php if ($data['UserType']=='SA') echo 'selected'; ?> value="SA">Supper Admin</option>
                    <option <?php if ($data['UserType']=='A') echo 'selected'; ?> value="A">Admin</option>
                    <option <?php if ($data['UserType']=='EO') echo 'selected'; ?> value="EO">Entry Operator</option>
                    <option <?php if ($data['UserType']=='E') echo 'selected'; ?> value="E">Editor</option>
                    <option <?php if ($data['UserType']=='SU') echo 'selected'; ?> value="SU">Supper User</option>
                    <option <?php if ($data['UserType']=='U') echo 'selected'; ?> value="U">User</option>
                  </select>
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Permission</label>
                
                <div class="form-group" style="border: 1px solid #CCCCCC; padding: 5px; border-radius:4px;">

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <td>
                          <label class="checkbox-inline">
                             
                              <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='user_create'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox1"value="user_create" onclick="test()"> Create User
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='user_view'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox2" value="user_view" onclick="test()"> View User Info
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='language'){ echo "checked='checked'";} }?> 
                            name="MenuPermission[]"  class="clschekbox" id="inlineCheckbox2" value="language" onclick="test()"> Language
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='menu'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox1" value="menu" onclick="test()"> Menu
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='header'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox2" value="header" onclick="test()"> Header
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='service'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]"  class="clschekbox" id="inlineCheckbox2" value="service" onclick="test()"> Service
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='download'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox1" value="download" onclick="test()"> Download
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='career'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox2" value="career" onclick="test()"> Career
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='testimonial'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox2" value="testimonial" onclick="test()"> Testimonial
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='followus'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox1" value="followus" onclick="test()"> Follow Us
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='partners'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox2" value="partners" onclick="test()"> Partners
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='clients'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox2" value="clients" onclick="test()"> Clients
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='certified'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox1" value="certified" onclick="test()"> Certified
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='userlist'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox2" value="userlist" onclick="test()"> User List
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='logreport'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox2" value="logreport" onclick="test()"> User Log Report
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='transreport'){ echo "checked='checked'";} }?>
                             name="MenuPermission[]" class="clschekbox" id="inlineCheckbox1" value="transreport" onclick="test()"> Transaction Reporting
                          </label>
                        </td>
                        <td>
                          <label class="checkbox-inline">
                              <input type="checkbox" <?php $m=$data['MenuPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='visitorveport'){ echo "checked='checked'";} }?>
                               name="MenuPermission[]" class="clschekbox" id="inlineCheckbox2" value="visitorveport" onclick="test()"> Visitor Report
                          </label>
                        </td>
                        <td>
                            <input type="hidden" name="menuaccess" id="menuaccess"/>
                                   
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>

                <div class="form-group" style="border: 1px solid #CCCCCC; padding: 5px; border-radius:4px;">
                  <label class="checkbox-inline">
                    <input type="checkbox" <?php $m=$data['WorkPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='add'){ echo "checked='checked'";} }?>
                     name="WorkPermission[]" class="wclschekbox" id="inlineCheckbox1" value="add" onclick="work()"> Add
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" <?php $m=$data['WorkPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='view'){ echo "checked='checked'";} }?>
                     name="WorkPermission[]" class="wclschekbox" id="inlineCheckbox2" value="view" onclick="work()"> View
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" <?php $m=$data['WorkPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='edit'){ echo "checked='checked'";} }?>
                     name="WorkPermission[]" class="wclschekbox" id="inlineCheckbox3" value="edit" onclick="work()"> Edit
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" <?php $m=$data['WorkPermission']; $access=explode(',',$m); foreach ($access as $tkey){if($tkey=='delete'){ echo "checked='checked'";} }?>
                     name="WorkPermission[]" class="wclschekbox" id="inlineCheckbox3" value="delete" onclick="work()"> Delete
                  </label>
                  <input type="hidden" name="workaccess" id="workaccess"/>
                </div>
              </div>

            </div>


          
        </div>

        <div class="row" style="padding: 5px 0px 15px 0px; font-size: 12px; text-align: center;">
          <button type="submit" name="update" class="btn btn-success">Update</button> 
        </div>
        </form>

