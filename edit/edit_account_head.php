<?php
date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
//$date        = date('Y-m-d');
$ip_add      = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;
$token = isset($_GET['token'])? $_GET['token']:NULL;

$details = $obj->details_by_cond("tbl_accounts_head","acc_id='$token'");

extract($details);

    if(isset($_POST['update'])){
        extract($_POST);

   $form_data=array(
          'acc_name' => $Name,
          'acc_type' => $type,
          'acc_desc' => str_replace("'", "", $Details),           
          'acc_status' => $status,  
           
          'entry_by' => $userid,       
          'entry_date' => $date_time,
          'update_by' => $userid                        
   );
    $branch_id=$obj->Update_data("tbl_accounts_head",$form_data,"where acc_id='$token'");
   
    if($branch_id){
        
        ?>
<script>
   window.location="?q=view_account_head";
 </script>
<?php                
    }
    else{
            echo $notification = 'Update Failed';
    }             
    }
?>

<div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
    <b><?php echo isset($notification)? $notification :NULL; ?></b>
</div>
<div class="row" style="padding:10px; font-size: 12px;">
    <form role="form" enctype="multipart/form-data" method="post">    
            <div class="row" style="padding:10px; font-size: 12px;">
                <div class="col-md-6">                       
                   <div class="form-group">
                        <label>Account Head Name</label>
                        <input value="<?php echo $details['acc_name']? $details['acc_name']:NULL; ?>" type="text" name="Name" class="form-control" id="ResponsiveTitle"  >
                   </div>
                    <div class="form-group">                                                         
                        <label>Account Head Type</label>
                        <select class="form-control" required="required" name="type" id="status">
                           <option <?php if($details['acc_type']=='1') echo 'selected';  ?>  value="1">Expense</option>
                           <option <?php if($details['acc_type']=='2') echo 'selected'  ?> value="2">Income</option>
                        </select>                       
                    </div> 
                    <div class="form-group">
                        <label>Account Head Details</label>
                         <textarea class="form-control" name="Details" id="ResponsiveDetelis" rows="6"><?php echo $details['acc_desc']? $details['acc_desc']:NULL; ?></textarea>
                     </div>

                    <div class="form-group">                                                         
                        <label>Status</label>
                        <select class="form-control" required="required" name="status" id="status">
                           <option <?php if($details['acc_status']=='1') echo 'selected';  ?>  value="1">Active</option>
                           <option <?php if($details['acc_status']=='0') echo 'selected'  ?> value="0">Inactive</option>
                        </select>                       
                    </div>                       
                </div>
                <div class="col-md-6"></div>
            </div>
            <div class="row" style="padding: 5px 0px 15px 25px; font-size: 12px;">
              <button type="submit" class="btn btn-success" name="update">Submit</button> 
            </div>
    </form>
</div>
<hr></hr>