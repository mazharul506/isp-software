<?php
date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
//$date        = date('Y-m-d');
$ip_add      = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;
$token = isset($_GET['token'])? $_GET['token']:NULL;

$details = $obj->details_by_cond("tbl_agent","ag_id='$token'");

extract($details);

    if(isset($_POST['update'])){
        extract($_POST);

   $form_data=array(
           
        'ag_name' => $Name,         
          'ag_mobile_no' => $mobile,
          'ag_office_address' => str_replace("'", "", $Details),
          'taka' => $taka,
          'mb' => $mb,
          'ag_email' => $email,
          'bill_cat' => $bill_cat,                               
          'ag_status' => $status,  
          'connect_charge' => $connect_charge,  
          'ip'=>$ip, 
          'entry_by' => $userid,
          'update_by' => $userid                      
   );
    $branch_id=$obj->Update_data("tbl_agent",$form_data,"where ag_id='$token'");
   
    if($branch_id){
        
        ?>
<script>
   window.location="?q=view_agent";
 </script>
<?php                
    }
    else{
            echo $notification = 'Update Failed';
    }             
    }
?>

<div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
    <b><?php echo isset($notification)? $notification :NULL; ?></b>
</div>
<div class="row" style="padding:10px; font-size: 12px;">
    <form role="form" enctype="multipart/form-data" method="post">    
                <div class="row" style="padding:10px; font-size: 12px;">

                    <div class="col-md-6">
                       
                        <div class="form-group">
                             <label>Customer Name</label>
                             <input value="<?php echo $details['ag_name']? $details['ag_name']:NULL; ?>" type="text" name="Name" class="form-control" id="ResponsiveTitle"  >
                        </div>                                                                 
                       <div class="form-group">
                             <label>Mobile No</label>
                             <input value="<?php echo $details['ag_mobile_no']? $details['ag_mobile_no']:NULL; ?>" type="text" name="mobile" class="form-control" id="ResponsiveTitle"  >
                        </div>
                       <div class="form-group">
                            <label>Address</label>
                             <textarea class="form-control" name="Details" id="ResponsiveDetelis" rows="6"><?php echo $details['ag_office_address']? $details['ag_office_address']:NULL; ?></textarea>
                        </div>
                                            
                        <div class="form-group">
                            <label>Amount Of Taka</label>
                            <input value="<?php echo $details['taka']? $details['taka']:NULL; ?>" type="text" name="taka" class="form-control" id="ResponsiveTitle"  >
                       </div>   
                         <div class="form-group">
                            <label>Connect Charge</label>
                            <input value="<?php echo $details['connect_charge']? $details['connect_charge']:NULL; ?>" type="text" name="connect_charge" class="form-control" id="ResponsiveTitle"  >
                       </div>
                        <div class="form-group">
                            <label>Amount Of MB</label>
                            <input value="<?php echo $details['mb']? $details['mb']:NULL; ?>" type="text" name="mb" class="form-control" id="ResponsiveTitle"  >
                       </div> 
						<div class="form-group">
                            <label>IP No</label>
                            <input type="text" name="ip" class="form-control" value="<?php echo $details['ip']? $details['ip']:NULL; ?>" id="ResponsiveTitle" required="required" >
                       </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input value="<?php echo $details['ag_email']? $details['ag_email']:NULL; ?>" type="email" name="email" class="form-control" id="ResponsiveTitle" required="required" >
                       </div>                                                                                                         
                        <div class="form-group">                                                         
                            <label>Bill Category</label>
                            <select class="form-control" required="required" name="bill_cat" id="status">
                                <option  value="">---Category Select---</option>
                                <option <?php if($details['bill_cat']=='1') echo 'selected';  ?> value="1">Monthly</option>
                                <option <?php if($details['bill_cat']=='2') echo 'selected';  ?> value="2">Half Yearly</option>
                                <option <?php if($details['bill_cat']=='3') echo 'selected';  ?> value="3">Yearly</option>
                            </select>                       
                         </div>                       
                        <div class="form-group">                                                         
                            <label>Status</label>
                            <select class="form-control" required="required" name="status" id="status">
                               <option <?php if($details['ag_status']=='1') echo 'selected';  ?>  value="1">Active</option>
                               <option <?php if($details['ag_status']=='0') echo 'selected'  ?> value="0">Inactive</option>
                            </select>                       
                        </div>
                        <div class="row" style="text-align: center; padding: 5px 0px 15px 25px; font-size: 12px;">
                            <button type="submit" class="btn btn-success" name="update">Update</button> 
                        </div>                                                                                  
                                             
                    </div>
                    <div class="col-md-6"></div>
                </div>
        </form>
</div>
<hr></hr>