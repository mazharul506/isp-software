<div class="col-md-2" style="padding:0px 10px 0px 0px;">

    <div class="col-md-12" style="padding:0px; width:100%;">

        <div id="body_service">
            <div id="body_service_list">

                <div id="body_service_list_text"> 
                    <ul class="box" style="margin:0px ! important;">

                        <li class="mactive">
                            <a href="?q=main_configuration" class="left_menu_bull" style=" padding: 1px 0px 1px 30px; font-size:14px;  color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-weight:600;">Dashboard</a>
                        </li>

                        <li  style="background:url('asset/img/left-menu-active.gif') repeat-x scroll 0 0 rgba(0, 0, 0, 0);">
                            <a  href="?q=main_configuration" class="left_menu_bull mainli" style="color:white;">Configuration</a>
                            <ul>
                            <?php if($ty=='SA'){ ?>
                                <li class="list">&nbsp;<a class="texta" href="?q=usercreate">User Create</a></li>
                            <?php 
                            }
                             if($ty=='SA' OR $ty=='A'){
                            ?>
                                <li class="list">&nbsp;<a class="texta" href="?q=view_user">View User Info</a></li>                                                              
                            <?php } ?>
                            </ul>
                        </li>
						
						<li class="mactive">
                            <a href="?q=view_agent" class="left_menu_bull" style=" padding: 1px 0px 1px 30px; font-size:14px;  color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-weight:600;">Customer Info</a>
							<ul>
                            <?php if($ty=='SA'){ ?>
                                <li class="list">&nbsp;<a class="texta" href="?q=add_agent">Add New Customer</a></li>
                                <li class="list">&nbsp;<a class="texta" href="?q=view_agent">Customer Info</a></li>
                                <li class="list">&nbsp;<a class="texta" href="?q=view_new_join_customer">New Customer</a></li>
                            <?php 
                            }
                            ?>
                            </ul>
                        </li>
                        <li style="background:url('asset/img/left-menu-active.gif') repeat-x scroll 0 0 rgba(0, 0, 0, 0);">
                            <a href="?q=main_configuration" class="left_menu_bull mainli" style="color:white;">Income</a>
                            <ul>
                                <?php                   
                                  if($ty=='SA' OR $ty=='A'){
                                 ?>                                                                                                                       
                                    <li class="list">&nbsp;<a class="texta" href="?q=view_due_payment">Bill Collection</a></li>                                                              
                                    <li class="list">&nbsp;<a class="texta" href="?q=view_all_dues">All Dues</a></li>                                                              
                                    <li class="list">&nbsp;<a class="texta" href="?q=view_conn_charge">Connection Charge</a></li>                                                              
                                     <li class="list">&nbsp;<a class="texta" href="?q=view_income">Others Income</a></li>                                                              
                                     <li class="list">&nbsp;<a class="texta" href="?q=income_report">Income Report</a></li>                                                                 
                                 <?php                                  
                                  } 
                                  ?>
                            </ul>
                        </li>
						
						
						
						
						<li style="background:url('asset/img/left-menu-active.gif') repeat-x scroll 0 0 rgba(0, 0, 0, 0);">
                            <a href="?q=main_configuration" class="left_menu_bull mainli" style="color:white;">Expense</a>
                            <ul>
                                <?php                   
                                  if($ty=='SA' OR $ty=='A'){
                                 ?>
                                    <li class="list">&nbsp;<a class="texta" href="?q=view_account_head">Account Head</a></li>  
									<li class="list">&nbsp;<a class="texta" href="?q=view_bonus">Discount</a></li>                                                              
                                     <li class="list">&nbsp;<a class="texta" href="?q=view_expense">Expense</a></li>                                                            
                                     <li class="list">&nbsp;<a class="texta" href="?q=expense_report">Expense Report</a></li>                                                              
                                     <li class="list">&nbsp;<a class="texta" href="?q=acc_statement">Account Statement</a></li>  
                                 <?php                                  
                                  } 
                                  ?>
                            </ul>
                        </li>
                        <li style="background:url('asset/img/left-menu-active.gif') repeat-x scroll 0 0 rgba(0, 0, 0, 0);">
                            <a href="?q=main_configuration" class="left_menu_bull mainli" style="color:white;">Balance Sheet</a>
                            <ul>
                                <?php                   
                                  if($ty=='SA' OR $ty=='A'){
                                 ?>
                                     <li class="list">&nbsp;<a class="texta" href="?q=monthly_new">Monthly Balance Report</a></li>                                                              
                                     <li class="list">&nbsp;<a class="texta" href="?q=yearly">Yearly Balance Report</a></li>                                                              
                                    <?php                                  
                                  } 
                                  ?>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>	
        </div>
    </div>		
</div>