<?php 
session_start(); 

$user_id = isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;
$FullName = isset($_SESSION['FullName']) ? $_SESSION['FullName']:NULL;
$UserName = isset($_SESSION['UserName'])? $_SESSION['UserName']:NULL;
$PhotoPath = isset($_SESSION['PhotoPath'])? $_SESSION['PhotoPath']:NULL;
$ty = isset($_SESSION['UserType'])? $_SESSION['UserType']:NULL; 


if (!empty($_SESSION['UserId'])) {

//========================================
include 'model/oop.php';
$obj = new Controller();
//========================================


//========================================
$wpdata = $obj->details_by_cond('vw_user_info',"UserId='$user_id'");
  if($wpdata){
  extract($wpdata);
}
$wp=isset($wpdata['WorkPermission'])? $wpdata['WorkPermission']:NULL; 
$acc=explode(',',$wp);
//========================================

define("home", "#");
//define("home", "http://localhost/office_last/admin/");
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>
        <?php 
        $q = isset($_GET['q']) ? $_GET['q']:NULL;
        if(empty($q)) { 
    	?>       
        <?php
    	}
    	else
    	{echo str_replace("_", " ",$q).' | Welcome Travel Software ';}
    	?>
        </title>
        <?PHP include 'include/js_cs_link.php';?>
    </head>

    <body onload="startTime()">
            <div class="container" id="content" style="width:100% !important; padding:0px; border:0px; background-color:#EAEAEA;">
                <div class="col-md-12" style="background:#100000; padding:0px; min-height:50px;">  

        <!-- ============== header ========================== -->
                    <?php include 'include/header.php';?>
        <!-- ============== header close ========================== -->
                </div>
                    <div class="col-md-12" style="margin:15px 0px 15px 0px; padding:0px 15px 0px 0px;">

                            <!-- ============== Start Menu ========================== -->
                    <?php include 'include/sidebar.php'; ?>
                            <!-- ============== End Menu ========================== -->


                            <!-- ============== Start Container ========================== -->
                            <div  id="editbodyload"class="col-md-10" style="  background:#FFFFFF; border:1px solid #999999;" id="bodyload">

                            <?php 
                                 if($q=='home') {
                                    include 'include/body.php';
                                 }

                                 // Insert Page Start
                                 elseif ($q=='usercreate') {
                                    include 'add/usercreate.php';
                                 }
                                 elseif ($q=='add_customer') {
                                    include 'add/add_customer.php';
                                 }
                                 elseif ($q=='add_branch') {
                                    include 'add/add_branch.php';
                                 }
                                 elseif ($q=='add_service') {
                                    include 'add/add_service.php';
                                 }
                                 elseif ($q=='add_airlines') {
                                    include 'add/add_airlines.php';
                                 }
                                 elseif ($q=='add_vendor') {
                                    include 'add/add_vendor.php';
                                 }
                                 elseif ($q=='add_account_head') {
                                    include 'add/add_account_head.php';
                                 }
                                 elseif ($q=='add_sector') {
                                    include 'add/add_sector.php';
                                 }
                                 elseif ($q=='add_agent') {
                                    include 'add/add_agent.php';
                                 }
                                 elseif ($q=='add_expense') {
                                    include 'add/add_expense.php';
                                 }
                                 elseif ($q=='add_income') {
                                    include 'add/add_income.php';
                                 }
                                 elseif ($q=='t_customer') {
                                    include 'add/t_customer.php';
                                 }
                                 elseif ($q=='t_agent') {
                                    include 'add/t_agent.php';
                                 }
                                 elseif ($q=='add_payment') {
                                    include 'add/add_payment.php';
                                 }


                                 // View Page Start
                                 elseif ($q=='view_user') {
                                    include 'view/view_user.php';
                                 }
                                 elseif ($q=='user_details') {
                                    include 'details/user_details.php';
                                 }               
                                 elseif ($q=='view_customer') {
                                    include 'view/view_customer.php';
                                 }               
                                 elseif ($q=='view_new_join_customer') {
                                    include 'view/view_new_join_customer.php';
                                 }               
                                 elseif ($q=='view_branch') {
                                    include 'view/view_branch.php';
                                 }               
                                 elseif ($q=='view_all_dues') {
                                    include 'view/view_all_dues.php';
                                 }               
                                 elseif ($q=='view_report_paganition') {
                                    include 'view/view_report_paganition.php';
                                 }               
                                 elseif ($q=='view_conn_charge') {
                                    include 'view/view_conn_charge.php';
                                 }               
                                 elseif ($q=='view_vendor') {
                                    include 'view/view_vendor.php';
                                 }               
                                 elseif ($q=='view_account_head') {
                                    include 'view/view_account_head.php';
                                 }               
                                 elseif ($q=='view_sector') {
                                    include 'view/view_sector.php';
                                 }               
                                 elseif ($q=='view_agent') {
                                    include 'view/view_agent.php';
                                 }               
                                 elseif ($q=='view_expense') {
                                    include 'view/view_expense.php';
                                 }
								 elseif ($q=='view_bonus') {
                                    include 'view/view_bonus.php';
                                 }               
                                 elseif ($q=='view_income') {
                                    include 'view/view_income.php';
                                 }               
                                 elseif ($q=='view_customer_payment') {
                                    include 'view/view_customer_payment.php';
                                 }               
                                 elseif ($q=='view_agent_payment') {
                                    include 'view/view_agent_payment.php';
                                 }  
                                 elseif ($q=='view_t_agent') {
                                    include 'view/view_t_agent.php';
                                 }  
                                 elseif ($q=='view_t_customer') {
                                    include 'view/view_t_customer.php';
                                 }  

                                 elseif ($q=='income_report') {
                                    include 'view/report/income_report.php';
                                 }               
                                 elseif ($q=='expense_report') {
                                    include 'view/report/expense_report.php';
                                 }
								 elseif ($q=='view_expense_details') {
                                    include 'view/view_expense_details.php';
                                 }               
                                 elseif ($q=='acc_statement') {
                                    include 'view/report/acc_statement.php';
                                 }               
                                 elseif ($q=='dailyReport') {
                                    include 'view/report/dailyReport.php';
                                 }               
                                 elseif ($q=='dueReport') {
                                    include 'view/report/dueReport.php';
                                 }               

                                 elseif ($q=='customer_info_report') {
                                    include 'view/reportall/customer_info_report.php';
                                 }               
                                 elseif ($q=='due_payment') {
                                    include 'view/reportall/due_payment.php';
                                 }               
                                 elseif ($q=='get_payment') {
                                    include 'view/reportall/get_payment.php';
                                 }               
                                 elseif ($q=='customer_payment_report') {
                                    include 'view/reportall/customer_payment_report.php';
                                 }               
                                 elseif ($q=='agent_payment_report') {
                                    include 'view/reportall/agent_payment_report.php';
                                 }               
                                 elseif ($q=='monthly') {
                                    include 'view/statement/monthly.php';
                                 }elseif ($q=='monthly_new') {
                                    include 'view/statement/monthly_new.php';
                                 }               
                                 elseif ($q=='yearly') {
                                    include 'view/statement/yearly.php';
                                 }               
                                 elseif ($q=='view_due_payment') {
                                    include 'view/view_due_payment.php';
                                 }               
                                 elseif ($q=='view_customer_payment_individual') {
                                    include 'view/view_customer_payment_individual.php';
                                 }               
                                 
                                 // Edit Page Start
                                elseif ($q=='user_edit') {
                                               include 'edit/user_edit.php';
                                            }

                                elseif($q=='user_ch_pass'){ 
                                   include 'edit/user_ch_pass.php'; 
                                }

                                elseif($q=='edit_branch'){ 
                                   include 'edit/edit_branch.php'; 
                                }
                                elseif($q=='edit_service'){ 
                                   include 'edit/edit_service.php'; 
                                }
                                elseif($q=='edit_airlines'){ 
                                   include 'edit/edit_airlines.php'; 
                                }
                                elseif($q=='edit_vendor'){ 
                                   include 'edit/edit_vendor.php'; 
                                }
                                elseif($q=='edit_account_head'){ 
                                   include 'edit/edit_account_head.php'; 
                                }
                                elseif($q=='edit_sector'){ 
                                   include 'edit/edit_sector.php'; 
                                }                 
                                elseif($q=='edit_customer'){ 
                                   include 'edit/edit_customer.php'; 
                                }                 
                                elseif($q=='edit_agent'){ 
                                   include 'edit/edit_agent.php'; 
                                }                 
                                elseif($q=='edit_expense'){ 
                                   include 'edit/edit_expense.php'; 
                                }                 
                                elseif($q=='edit_income'){ 
                                   include 'edit/edit_income.php'; 
                                }                 
                                elseif($q=='edit_customer_payment'){ 
                                   include 'edit/edit_customer_payment.php'; 
                                }                 
                                elseif($q=='edit_agent_payment'){ 
                                   include 'edit/edit_agent_payment.php'; 
                                }                 
                                elseif($q=='edit_t_agent'){ 
                                   include 'edit/edit_t_agent.php'; 
                                }                 
                                elseif($q=='edit_t_customer'){ 
                                   include 'edit/edit_t_customer.php'; 
                                }                 
                                  // Edit Page End  

                                 else{
                                    include 'include/body.php';
                                 }
                            ?>
                            </div>
                            <!-- ============== End Container ========================== -->		
                    </div>	
                    <div class="col-md-12" id="footer" class="box">
                            <p class="f-right" style="font-size:11px;">&copy; <?php echo date('Y'); ?> <a href="#" target="_blank">ISP Company Software.</a>, All Rights Reserved</p>
                            <p class="f-left"></p>
                    </div>			
            </div>
    </body>
</html>
<?php
 }
     else{ header("location: include/login.php");}        
?>
