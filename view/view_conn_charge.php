  <style>
	  
    .ui-autocomplete {
      padding: 0;
      list-style: none;
      background-color: #fff;
      width: 218px;
      border: 1px solid #B0BECA;
      max-height: 350px;
      overflow-x: hidden;
    }
    .ui-autocomplete .ui-menu-item {
      border-top: 1px solid #B0BECA;
      display: block;
      padding: 4px 6px;
      color: #353D44;
      cursor: pointer;
    }
    .ui-autocomplete .ui-menu-item:first-child {
      border-top: none;
    }
    .ui-autocomplete .ui-menu-item.ui-state-focus {
      background-color: #D5E5F4;
      color: #161A1C;
    }
	.full{
		padding:10px;
	}
	.test{
		background-color:white;
		width:40%;
		height:70px;
		padding:20px;
		margin-left:30%;
		color:black;
		font-weight:900;
		font-size:24px;
	}
	#con{
		margin-bottom:10px;
	}
	#move{
		position:absolute;
		left:43%;
		bottom:-1%;
		z-index:999;
		-webkit-transition:1s;
	}
	</style>
<?php
date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
$date =date('Y-m-d');
//$date        = date('Y-m-d');
$Month=date('m',strtotime($date));
$Year=date('Y',strtotime($date));
$ip_add = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;


//=====================start==============================


//=======================end============================

?>


<!------------------------------------------>


<!------------------------------------------>



 <div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
        <b><?php echo isset($notification)? $notification :NULL; ?></b>
 </div>
<div class="col-md-12" style=" background:#606060; margin-top:20px; margin-bottom: 15px; min-height:45px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">
    <div class="col-md-6">
         <b>Vie All Connection Charge Information</b>
    </div>               
</div>
 <div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
        <b><?php echo isset($notification)? $notification :NULL; ?></b>
 </div>
<div class="col-md-12" style=" background:#606060; margin-top:20px; margin-bottom: 15px; min-height:45px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">   
        <form  action="" method="POST">
            <div class="col-md-5">
                 <div class="form-group">
                    <label class="col-sm-6" >Month</label>
                    <input type="date" class="form-control datepicker" placeholder="Date" name="dateform" id="new_flight_date" required>

                 </div>              
            </div>
            <div class="col-md-5">
                 <div class="form-group">
                    <label class="col-sm-6" >Month</label>
                    <input style="color: black;" id="old_flight_date" type="date" class="form-control datepicker" placeholder="Date" name="dateto" required>

                 </div>              
            </div>
            <div class="col-md-2" style="margin-top: 30px;">
               <button type="submit"  name="search" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button> 
            </div>
        </form>                  
</div>
<div class="row" style="padding:10px; font-size: 12px;"> 
<?php
$flag=isset($_GET['key'])?"1":"0";
$where="";
$title=" ".date("M, Y");;
    if(isset($_POST['search'])){
      extract($_POST);
	  $flag="1";
	  $title="".$dateform." to ".$dateto;
	  $where="entry_date BETWEEN '$dateform' and '$dateto'";
    }
	//entry_date BETWEEN '$dateform1' and '$dateto1' and acc_type='1'
?>
<!-- here end table -->
<?php
if($flag=="0"){
	$where="MONTH(entry_date)='$Month' and YEAR(entry_date)='$Year'";
}
?>
<!--
<div class="col-md-1 col-md-offset-11" >
		<button type="submit" class="btn btn-primary pull-right" onclick="printDiv('month_print')"  >Print Statement</button>
</div> -->
<div class="row" id="">

	<div class="col-md-11" >
		<h2 style="text-align:center;"><?=$title?></h2>
	</div>
		<div class="col-md-12" id="month_print">
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped" id="example">
                <thead> 
                    <tr>
                        <th>Date</th> 
						<th>Customer Name</th>
                        <th>Address</th>
                        <th>Mobile No</th>                       
                        <th>Speed Amount</th>                     
                        <th>IP</th>                      
                        <th>Connection Charge</th>
                    </tr>                     
                       
                </thead>
				<tbody>
                   <?php
                    $total=0;
                    foreach ($obj->view_all_by_cond("tbl_agent",$where." ORDER BY entry_date DESC") as $value){
                                                                                
                    ?>
                    <tr>
						<td><?php echo date("d-m-Y", strtotime(isset($value['entry_date'])?$value['entry_date']:"2016-02-1"));?></td>
						<td>
							<a href="?q=view_customer_payment_individual&token2=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL?>" ><?php echo isset($value['ag_name'])?$value['ag_name']:NULL;?></a>
						</td>
						<td><?php echo isset($value['ag_office_address'])?$value['ag_office_address']:NULL;?></td>
						<td><?php echo isset($value['ag_mobile_no'])?$value['ag_mobile_no']:NULL;?></td>
						<td><?php echo isset($value['mb'])?$value['mb']:NULL;?></td>
						<td><?php echo isset($value['ip'])?$value['ip']:NULL;?></td>
						<td><?php echo isset($value['connect_charge'])?number_format($value['connect_charge'],"2",".",""):NULL;?></td>
                    </tr>
                    <?php
					$total+=isset($value['connect_charge'])?$value['connect_charge']:0;
                    }
                    ?>
				</tbody>	
				<tfoot>
					<tr style="font-size:18px;font-weight:900;">
						<td colspan="6" style="text-align:right;"><b>Grand Total</b></td>
						<td><b><?=number_format($total,"2",".","")?></b></td>
					</tr>
				</tfoot>
            </table>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {

} );
function printDiv(divName) {
	$(".print").css("margin-left","0px");
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<!-- here end table -->

	

