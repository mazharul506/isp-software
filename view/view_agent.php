<?php
date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
//$date        = date('Y-m-d');
$key=isset($_GET['key'])? $_GET['key'] :"all";
$ip_add = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;


$intoken = isset($_GET['intoken'])? $_GET['intoken'] :NULL;
$actoken = isset($_GET['actoken'])? $_GET['actoken'] :NULL;


if (!empty($intoken)) {
    $form_data=array('ag_status' => '0','update_by' => $userid );

    $obj->Update_data("tbl_agent", $form_data, "where ag_id='$intoken'");
}

if (!empty($actoken)) {
    $form_data=array('ag_status' => '1','update_by' => $userid );

    $obj->Update_data("tbl_agent", $form_data, "where ag_id='$actoken'");
}


// ========== Delete Function Start =================
$dltoken = isset($_GET['dltoken'])? $_GET['dltoken']:NULL;
if(!empty($dltoken)){

$dele = $obj->Delete_data("tbl_agent","ag_id='$dltoken'");

if(!$dele)
    {$notification = 'Delete Successfull';}
else
    {$notification = 'Delete Failed';} 
}
// ========== Delete Function End =================

?>

 <div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
        <b><?php echo isset($notification)? $notification :NULL; ?></b>
 </div>
<div class="col-md-12" style=" background:#606060; margin-top:20px; margin-bottom: 15px; min-height:45px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">
    <div class="col-md-6">
         <b>View Customer Information</b>
    </div>           
    <div class="col-md-6" style="">
       <?php if($ty=='SA'){ ?>
        <a class="addbutton" href="?q=add_agent">ADD NEW<span class="glyphicon glyphicon-plus"></span></a>
        <?php } ?> 
    </div>
</div>

<?php
if($key=="all"){
?>
<div class="col-md-12" style="margin-bottom:10px;">
	<ul class="nav nav-pills">
	  <li role="presentation" class="active"><a href="?q=view_agent&key=all">All</a></li>
	  <li role="presentation"><a href="?q=view_agent&key=active">Active</a></li>
	  <li role="presentation"><a href="?q=view_agent&key=inactive">Inactive</a></li>
	</ul>
</div>
<!-- all user show -->
<div id="div_all" class="row" style="padding:10px;font-size: 12px;">       
    <div class="col-md-12">
        <div class="table-responsive">
            <table style="margin-left:-15px;" class="table table-bordered table-hover table-striped" id="example">
                <thead> 
                    <tr>
						<th>Customer ID</th>
                        <th>Customer Name</th>
                        <th>Address</th>
                        <th>Mobile No</th>                       
                        <th>Speed</th>                        
                        <th>Amount of Taka</th>                        
                        <th>Connection Charge</th>                        
                        <th>IP</th>
                        <th>Billing Category</th>                      
                        <th>Action</th>
                    </tr>
                </thead>
                   <?php
                    $i='0';
                    foreach ($obj->view_all("tbl_agent") as $value){
                        $i++;                                                              
                    ?>
                    <tr>
                    
						<td>
							<a href="?q=view_customer_payment_individual&token2=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL?>" ><?php echo isset($value['cus_id'])?$value['cus_id']:NULL;?></a>
						</td>
						<td><?php echo isset($value['ag_name'])?$value['ag_name']:NULL;?></td>
						<td><?php echo isset($value['ag_office_address'])?$value['ag_office_address']:NULL;?></td>
						<td><?php echo isset($value['ag_mobile_no'])?$value['ag_mobile_no']:NULL;?></td>
						<td><?php echo isset($value['mb'])?$value['mb']:NULL;?></td>
						<td><?php echo isset($value['taka'])?$value['taka']:NULL;?></td>
						<td><?php echo isset($value['connect_charge'])?$value['connect_charge']:NULL;?></td>
						<td><?php echo isset($value['ip'])?$value['ip']:NULL;?></td>
						<td>
							<?php 
							if($value['bill_cat']=='1'){echo 'Monthly';}
							else if($value['bill_cat']=='2'){echo 'Half Yearly';}
							else if($value['bill_cat']=='3'){echo 'Yearly';}
							
							?>
						</td>
											 
						<td>
							<div class="btn-group">                                                                        
								<a class="btn btn-xs btn-info" style="padding:5px;" href="?q=edit_agent&token=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL?>">
								   <span class="glyphicon glyphicon-edit"></span>
								</a>                            
								<a href="?q=view_agent&dltoken=<?php echo isset($value['ag_id'])? $value['ag_id'] :NULL; ?>" class="btn btn-xs btn-danger" style="padding:5px;">
								   <span class="glyphicon glyphicon-remove"></span>
								</a>                        
							</div>                              
						</td>
                    </tr>
                    <?php
                    }
                    ?>                   
                </table>
            </div>
    </div>
</div>
<?php
}else if($key=="active"){

?>
<div class="col-md-12" style="margin-bottom:10px;">
	<ul class="nav nav-pills">
	  <li role="presentation" ><a href="?q=view_agent&key=all">All</a></li>
	  <li role="presentation" class="active"><a href="?q=view_agent&key=active">Active</a></li>
	  <li role="presentation"><a href="?q=view_agent&key=inactive">Inactive</a></li>
	</ul>
</div> 
<!-- div for all active user -->
<div id="div_inactive" class="row" style="padding:10px; font-size: 12px;">         
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="example" style="margin-left:-15px;" class="table table-bordered table-hover table-striped" >
                <thead> 
                    <tr>
                        <th>Customer ID</th>
                        <th>Customer Name</th>
                        <th>Address</th>
                        <th>Mobile No</th>                       
                        <th>Speed</th>                        
                        <th>Amount of Taka</th>                        
                        <th>Connection Charge</th>
                        <th>Billing Category</th>                        
                        <th>IP</th>                      
                        <th>Action</th>
                    </tr>
                </thead>
                   <?php
                    $i='0';
                    foreach ($obj->view_all_by_cond("tbl_agent","ag_status='1'") as $value){
                        $i++;                                                              
                    ?>
                    <tr>
                    <td>
                        <a href="?q=view_customer_payment_individual&token2=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL?>" ><?php echo isset($value['cus_id'])?$value['cus_id']:NULL;?></a>
                    </td>
                    <td><?php echo isset($value['ag_name'])?$value['ag_name']:NULL;?></td>
                    <td><?php echo isset($value['ag_office_address'])?$value['ag_office_address']:NULL;?></td>
                    <td><?php echo isset($value['ag_mobile_no'])?$value['ag_mobile_no']:NULL;?></td>
                    <td><?php echo isset($value['mb'])?$value['mb']:NULL;?></td>
                    <td><?php echo isset($value['taka'])?$value['taka']:NULL;?></td>
                    <td><?php echo isset($value['connect_charge'])?$value['connect_charge']:NULL;?></td>
                    <td>
                        <?php 
                        if($value['bill_cat']=='1'){echo 'MOnthly';}
                        else if($value['bill_cat']=='2'){echo 'Half Yearly';}
                        else if($value['bill_cat']=='3'){echo 'Yearly';}
                        
                        ?>
                    </td>
                    <td style="text-align: center;">
                    <?php 
                       echo isset($value['ip'])? $value['ip']:NULL;
                    ?>
                    </td>
                                         
                    <td>
                        <div class="btn-group">                                                                        
							<a class="btn btn-xs btn-info" style="padding:5px;" href="?q=edit_agent&token=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL?>">
							   <span class="glyphicon glyphicon-edit"></span>
							</a>                            
							<a href="?q=view_agent&dltoken=<?php echo isset($value['ag_id'])? $value['ag_id'] :NULL; ?>" class="btn btn-xs btn-danger" style="padding:5px;">
							   <span class="glyphicon glyphicon-remove"></span>
							</a>                        
						</div>                               
                    </td>
                    </tr>
                    <?php
                    }
                    ?>                   
                </table>
            </div>
    </div>
</div>
<?php 
}else if($key=="inactive"){
?>
<div class="col-md-12" style="margin-bottom:10px;">
	<ul class="nav nav-pills">
	  <li role="presentation" ><a href="?q=view_agent&key=all">All</a></li>
	  <li role="presentation" ><a href="?q=view_agent&key=active">Active</a></li>
	  <li role="presentation" class="active"><a href="?q=view_agent&key=inactive">Inactive</a></li>
	</ul>
</div>
<!-- div for all Inactive user -->
<div id="div_inactive" class="row" style="padding:10px; font-size: 12px;">         
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="example" style="margin-left:-15px;" class="table table-responsive table-bordered table-hover table-striped">
                <thead> 
                    <tr>
                        <th>Customer ID</th>
                        <th>Customer Name</th>
                        <th>Address</th>
                        <th>Mobile No</th>                       
                        <th>Speed</th>                        
                        <th>Amount of Taka</th>                        
                        <th>Connection Charge</th>
                        <th>Billing Category</th>                        
                        <th>IP</th>                      
                        <th>Action</th>
                    </tr>
                </thead>
                   <?php
                    $i='0';
                    foreach ($obj->view_all_by_cond("tbl_agent","ag_status='0'") as $value){
                        $i++;                                                              
                    ?>
                    <tr>
					<td>
                        <a href="?q=view_customer_payment_individual&token2=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL?>" ><?php echo isset($value['cus_id'])?$value['cus_id']:NULL;?></a>
                    </td>
                    <td><?php echo isset($value['ag_name'])?$value['ag_name']:NULL;?></td>
                    <td><?php echo isset($value['ag_office_address'])?$value['ag_office_address']:NULL;?></td>
                    <td><?php echo isset($value['ag_mobile_no'])?$value['ag_mobile_no']:NULL;?></td>
                    <td><?php echo isset($value['mb'])?$value['mb']:NULL;?></td>
                    <td><?php echo isset($value['taka'])?$value['taka']:NULL;?></td>
                    <td><?php echo isset($value['connect_charge'])?$value['connect_charge']:NULL;?></td>
                    <td>
                        <?php 
                        if($value['bill_cat']=='1'){echo 'MOnthly';}
                        else if($value['bill_cat']=='2'){echo 'Half Yearly';}
                        else if($value['bill_cat']=='3'){echo 'Yearly';}
                        
                        ?>
                    </td>
                    <td style="text-align: center;">
                     <?php 
                       echo isset($value['ip'])? $value['ip']:NULL;
                    ?>
                    </td>
                                         
                    <td>
                       <div class="btn-group">                                                                        
							<a class="btn btn-xs btn-info" style="padding:5px;" href="?q=edit_agent&token=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL?>">
							   <span class="glyphicon glyphicon-edit"></span>
							</a>                            
							<a href="?q=view_agent&dltoken=<?php echo isset($value['ag_id'])? $value['ag_id'] :NULL; ?>" class="btn btn-xs btn-danger" style="padding:5px;">
							   <span class="glyphicon glyphicon-remove"></span>
							</a>                        
						</div>                               
                    </td>
                    </tr>
                    <?php
                    }
                    ?>                   
                </table>
            </div>
    </div>
</div>
<?php
}
?>