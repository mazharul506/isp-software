<?php
date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
$date =date('Y-m-d');
//$date        = date('Y-m-d');
$ip_add = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;
$alldue1 =isset($_SESSION['alldue']) ? $_SESSION['alldue']:NULL;
                                
//=====================start==============================

function convert_number_to_words($number) {
   
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
   
    if (!is_numeric($number)) {
        return false;
    }
   
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
   
    $string = $fraction = null;
   
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
   
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
   
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
   
    return $string;
}

//=======================end============================

?>
<script>

function printDiv(divName) {
	$("table").find('a').each(function(){
	  $(this).attr('href','');
	});
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
function marginchange() {
	$(".print").css("margin-left","-30px");
}
</script>

 <div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
        <b><?php echo isset($notification)? $notification :NULL; ?></b>
 </div>
<div class="col-md-12" style=" background:#606060; margin-top:20px; margin-bottom: 15px; min-height:45px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">
    <div class="col-md-6">
         <b>Account Balance Sheet </b>
    </div>               
</div>
 <?php
  $dateform="";
  $dateto="";
  $pre_dateform="";
  $pre_dateto="";

    if(isset($_POST['search'])){
	extract($_POST);
		$pre_date=date_format(date_create($dateto."-".$dateform."-1"),"Y/m/d");
		$pre_dateform=date('n', strtotime('-1 months', strtotime($pre_date)));
		$pre_dateto=date('Y', strtotime('-1 months', strtotime($pre_date)));
		
	}else{
		$pre_dateform=date("n",strtotime("-1 months"));
		$pre_dateto=date("Y",strtotime("-1 months"));
		
		$dateform=date("n");
		$dateto=date("Y");
	}
	$dateObj   = DateTime::createFromFormat('!m', $dateform);
	$mon=$dateObj->format('F');
 ?>

<div class="col-md-12" onmouseover="marginchange()" style=" background:#606060; margin-top:20px; margin-bottom: 15px; min-height:45px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">   
        <form  action="" method="POST">
            <div class="col-md-5">
                 <div class="form-group">
                    <label class="col-sm-6" >Month</label>
                    <select class="form-control" name="dateform" id="status" required>
                        <option value="<?=$dateform?>" selected><?=$mon?></option>                                  
                        <option  value="1">January</option>
                        <option  value="2">February</option>
                        <option  value="3">March</option>
                        <option  value="4">April</option>
                        <option  value="5">May</option>
                        <option  value="6">June</option>
                        <option  value="7">July</option>
                        <option  value="8">August</option>
                        <option  value="9">September</option>
                        <option  value="10">October</option>
                        <option  value="11">November</option>
                        <option  value="12">December</option>                                     
                    </select>
                 </div> 
                
            </div>            
            <div class="col-md-5">
                 <div class="form-group">
                    <label class="col-sm-6" >Year</label>
                    <select class="form-control" name="dateto" id="status" required>
                        <option  value="<?=$dateto?>" selected><?=$dateto?></option>                                  
                        <option  value="2015">2015</option>
                        <option  value="2016">2016</option>
                        <option  value="2017">2017</option>
                        <option  value="2018">2018</option>
                        <option  value="2019">2019</option>
                        <option  value="2020">2020</option>
                        <option  value="2021">2021</option>
                        <option  value="2022">2022</option>
                        <option  value="2023">2023</option>
                        <option  value="2024">2024</option>
                        <option  value="2025">2025</option>                                
                    </select>
                 </div>                 
            </div>            
            <div class="col-md-2" style="margin-top: 30px;">
               <button type="submit"  name="search" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button> 
            </div>
        </form>                  
</div>
<!--
<div class="row" onmouseover="marginchange()">
	<div class="col-md-1 col-md-offset-10" >
		<button type="submit" class="btn btn-primary btn-lg pull-right" onclick="printDiv('month_table')"  >Print Statement</button>
	</div>
</div> -->

<div class="container" id="month_table" onmouseover="marginchange()" style="margin-left:0px;">

<h2 class="text-center">Expense Sheet of 
<?php 
$dateObj   = DateTime::createFromFormat('!m', $dateform);
echo $dateObj->format('F')." ".$dateto;

?></h2>

<div class="row" style="font-size:14px;">
<div class="col-md-8 col-md-offset-2">
	<table class="table table-responsive table-bordered table-hover table-striped">
		<thead> 
			<tr>
				<th>SL No.</th> 
				<th>Account Head</th> 
				<th style="text-align:center;">Expense</th>                                            																   
			</tr>
		</thead> 
		<tbody>
			<?php
			$total_expense=0;
			$i=1;
			foreach ($obj->get_all_expense($dateform,$dateto) as $customer_info){
				$total_expense+=$customer_info['payments'];
			?>
		  <tr >   
			  <td><?=$i++?></td>  
			  <td  style="text-align: left;">
				<a href="?q=view_expense_details&ex_id=<?=$customer_info['acc_head']?>&ex_name=<?=$customer_info['name']?>&dateTo=<?=$dateto?>&dateFrom=<?=$dateform?>">
				<?=$customer_info['name']?>
				</a>
			  </td>                                 
			  <td  style="text-align: right;"><?php echo number_format($customer_info['payments'], 2, '.', '');?></td>                                 
		  </tr>    
		  <?php
			}
		  ?>

		<tr style='height:33px;'>
			<td colspan="2" style='font-weight:300;font-size:18px;text-align: right;'>Total Expense</td>
			<td style='font-weight:300;font-size:18px;text-align: right;'><?=number_format($total_expense, 2, '.', '')?></td>
		</tr>

		</tbody>
	</table>
</div>
</div>