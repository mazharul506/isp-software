  <style>
	  
    .ui-autocomplete {
      padding: 0;
      list-style: none;
      background-color: #fff;
      width: 218px;
      border: 1px solid #B0BECA;
      max-height: 350px;
      overflow-x: hidden;
    }
    .ui-autocomplete .ui-menu-item {
      border-top: 1px solid #B0BECA;
      display: block;
      padding: 4px 6px;
      color: #353D44;
      cursor: pointer;
    }
    .ui-autocomplete .ui-menu-item:first-child {
      border-top: none;
    }
    .ui-autocomplete .ui-menu-item.ui-state-focus {
      background-color: #D5E5F4;
      color: #161A1C;
    }
	.full{
		padding:10px;
	}
	.test{
		background-color:white;
		width:40%;
		height:70px;
		padding:20px;
		margin-left:30%;
		color:black;
		font-weight:900;
		font-size:24px;
	}
	#con{
		margin-bottom:10px;
	}
	#move{
		position:absolute;
		left:43%;
		bottom:-1%;
		z-index:999;
		-webkit-transition:1s;
	}
	</style>
<?php
date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
$date =date('Y-m-d');
//$date        = date('Y-m-d');
$Month=date('m',strtotime($date));
$Year=date('Y',strtotime($date));
$ip_add = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;


//=====================start==============================


//=======================end============================

?>


<!------------------------------------------>


<!------------------------------------------>



 <div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
        <b><?php echo isset($notification)? $notification :NULL; ?></b>
 </div>
<div class="col-md-12" style=" background:#606060; margin-top:20px; margin-bottom: 15px; min-height:45px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">
    <div class="col-md-6">
         <b>New Joining Customer Information</b>
    </div>               
</div>
 <div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
        <b><?php echo isset($notification)? $notification :NULL; ?></b>
 </div>
<div class="col-md-12" style=" background:#606060; margin-top:20px; margin-bottom: 15px; min-height:45px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">   
        <form  action="" method="POST">
            <div class="col-md-5">
                 <div class="form-group">
                    <label class="col-sm-6" >Month</label>
                    <input type="date" class="form-control datepicker" placeholder="Date" name="dateform" id="new_flight_date" required>

                 </div>              
            </div>
            <div class="col-md-5">
                 <div class="form-group">
                    <label class="col-sm-6" >Month</label>
                    <input style="color: black;" id="old_flight_date" type="date" class="form-control datepicker" placeholder="Date" name="dateto" required>

                 </div>              
            </div>
            <div class="col-md-2" style="margin-top: 30px;">
               <button type="submit"  name="search" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button> 
            </div>
        </form>                  
</div>
<div class="row" style="padding:10px; font-size: 12px;"> 
<?php
$flag=isset($_GET['key'])?"1":"0";
$where="";
$title=" ".date("M, Y");;
    if(isset($_POST['search'])){
      extract($_POST);
	  $flag="1";
	  $title="New Joining Customer BETWEEN ".$dateform." and ".$dateto;
	  $where="entry_date BETWEEN '$dateform' and '$dateto'";
    }
	//entry_date BETWEEN '$dateform1' and '$dateto1' and acc_type='1'
?>
<!-- here end table -->
<?php
if($flag=="0"){
	$where="MONTH(entry_date)='$Month' and YEAR(entry_date)='$Year'";
}
?>
<div class="col-md-1 col-md-offset-11" >
		<button type="submit" class="btn btn-primary pull-right" onclick="printDiv('month_print')"  >Print Statement</button>
</div>
<div class="row" id="">

	<div class="col-md-11" >
		<h2 style="text-align:center;"><?=$title?></h2>
	</div>
		<?php
		 $all=$obj->Total_Count("tbl_agent",$where);
		 $finished=$obj->Total_Count("tbl_agent",$where." AND ag_status='1'");
		 $cancel=$obj->Total_Count("tbl_agent",$where." AND ag_status='0'");
		 
		?>
	<div class="col-md-12" id="con">
		<div class="row text-center">
			<div class="col-md-4 btn-info text-center full" id="all">
				<div class="img-circle test"><?=$all?></div>
				<h3>All</h3>
				<img src="./include/tri.png" width="50" height="25"  id="move">
			</div>
			<div class="col-md-4 text-center full" style="background-color:green;" id="finished">
				<div class="img-circle test"><?=$finished?></div>
				<h3>Active</h3>
			</div>
			<div class="col-md-4 btn-danger text-center full" id="cancel">
				<div class="img-circle test"><?=$cancel?></div>
				<h3>Inactive</h3>
			</div>
		</div>
	</div>
		<div class="col-md-12" id="month_print">
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped" id="example">
                <thead> 
                    <tr>
						<th>Customer Name</th>
                        <th>Address</th>
                        <th>Mobile No</th>                       
                        <th>Speed Amount</th>                        
                        <th>Amount of Taka</th>                        
                        <th>Connection Charge</th>
                        <th>Billing Category</th>
                        <th style="display:none;">Status</th>                       
                        <th>IP</th>                       
                        <th>Action</th>
                    </tr>                     
                       
                </thead>
				<tbody>
                   <?php
                    $i='0';
                    foreach ($obj->view_all_by_cond("tbl_agent",$where." ORDER BY entry_date DESC") as $value){
                        $i++;                                                              
                    ?>
                    <tr>
					
                    <td>
                        <a href="?q=view_customer_payment_individual&token2=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL?>" ><?php echo isset($value['ag_name'])?$value['ag_name']:NULL;?></a>
                    </td>
                    <td><?php echo isset($value['ag_office_address'])?$value['ag_office_address']:NULL;?></td>
                    <td><?php echo isset($value['ag_mobile_no'])?$value['ag_mobile_no']:NULL;?></td>
                    <td><?php echo isset($value['mb'])?$value['mb']:NULL;?></td>
                    <td><?php echo isset($value['taka'])?$value['taka']:NULL;?></td>
                    <td><?php echo isset($value['connect_charge'])?$value['connect_charge']:NULL;?></td>
                    <td>
                        <?php 
                        if($value['bill_cat']=='1'){echo 'Monthly';}
                        else if($value['bill_cat']=='2'){echo 'Half Yearly';}
                        else if($value['bill_cat']=='3'){echo 'Yearly';}
                        
                        ?>
                    </td>
                    <td style="text-align: center;display:none;">
                    <?php 
                        if ((isset($value['ag_status'])? $value['ag_status']:NULL)=='1') {
                    ?>
                    <a href="?q=view_agent&intoken=<?php echo isset($value['ag_id'])? $value['ag_id'] :NULL; ?>" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>Active</a>
                    
                    <?php 
                        }
                        else
                            {
                    ?>
                      <a href="?q=view_agent&actoken=<?php echo isset($value['ag_id'])? $value['ag_id'] :NULL; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-remove btn-danger"></span>Inactive</a>
                    <?php } ?>
                    </td>
                    <td><?php echo isset($value['ip'])?$value['ip']:NULL;?></td>                     
                    <td style="width:15%">
                        <div class="btn-group">                                                                         
                            <a class="btn btn-info" style="" href="?q=edit_agent&token=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL?>">
                               <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="?q=add_payment&token1=<?php echo isset($value['ag_id'])? $value['ag_id'] :NULL; ?>" class="btn btn-success">
								 <span class="glyphicon glyphicon-usd"></span> Edit
							</a>                         
                        </div>                              
                    </td>
                    </tr>
                    <?php
                    }
                    ?>                   
                </table>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$('#all').click(function () {
		$("#move").css("left","43%");
		var rows = $("tbody").find("tr").hide();
		var data = "".split(" ");
		$.each(data, function(i, v) {
			rows.filter(":contains('" + v + "')").show();
		});
	});
	$('#finished').click(function () {
		$("#move").css("left","143%");
		var rows = $("tbody").find("tr").hide();
		var data = "Active".split(" ");
		$.each(data, function(i, v) {
			rows.filter(":contains('" + v + "')").show();
		});
	});
	$('#cancel').click(function () {
		$("#move").css("left","243%");
		var rows = $("tbody").find("tr").hide();
		var data = "Inactive".split(" ");
		$.each(data, function(i, v) {
			rows.filter(":contains('" + v + "')").show();
		});
	});
} );
function printDiv(divName) {
	$(".print").css("margin-left","0px");
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<!-- here end table -->

	

