<?php
date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
$date        = date('Y-m-d');
$ip_add = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;
$notification="";
//taking month and years
$day=date('M-Y');

// get every customer dues query by id
//This method calculate all the dues pass by customer numeric id
//return a dues
function get_customer_dues($id){
			$obj = new Controller();
			$details = $obj->details_by_cond("tbl_agent","ag_id='$id'");
            extract($details);
            $bday1=$details['entry_date'];
            $bday = new DateTime($bday1);
            $today = new DateTime(date('Y-m-d', time())); // for testing purposes
            $diff = $today->diff($bday);
//            printf('%d years, %d month, %d days', $diff->y, $diff->m, $diff->d);
            $total=0;      
            $serviceamount1=0;
			foreach ($obj->view_all_by_cond("tbl_agent","ag_id='$id'") as $details1){
				extract($details1);
				$serviceamount1+=($details1['taka']);
            }
            if($diff->m!=0){
				$serviceamount2=($serviceamount1*$diff->y*12)+($serviceamount1*$diff->m);
            }
            else{
               $serviceamount2=$serviceamount1;
            }
            foreach ($obj->view_all_by_cond("vw_account","agent_id='$id' order by acc_id") as $customer_info){
				extract($customer_info);
				$total+=$customer_info['acc_amount'];
            }
			$bonus=0;
			foreach ($obj->view_all_by_cond("bonus","customerID='$id' ") as $customer_bonus){
				extract($customer_bonus);
				$bonus+=$customer_bonus['amount'];
            }       
			return $dueamount=$serviceamount2-($total+$bonus);
}
//Ends dues calculations

//check_bill_create_or_not
$cnt=$obj->Total_Count("monthly_bill_making_check","month_year='$day'");
if($cnt!=1){
	// pull all the customer in bill collection 
	//weather the have no dues or paid
	//in next step we erase those customer who have no dues.
    $form_data=array(                                                
         'pay_status' =>1, //Customer didn't pay yet   
		 'due_status' =>0 //no dues
   );
    $obj->Update_data("tbl_agent",$form_data,"pay_status!=5");
	
	$form_data2=array(                                                
         'month_year' =>$day //Insert current month date for future check weather bill is created or notification                                      
   );
	$obj->Insert_data("monthly_bill_making_check",$form_data2);
}
	
	
// ========== Now remove the customer from bill collection =================
// ========== Who paid already or no dues =================
foreach($obj->view_all_by_cond("tbl_agent","ag_status='1'") as $detailsid){
	extract($detailsid);
	$id=$detailsid['ag_id'];
	$dues=get_customer_dues($id);
	//customer remove from bill collection list who have advanced paid or no dues
	if($dues<=0){
		$form_data=array(                                                
			'pay_status' =>0, //Customer paid  
			'due_status' =>0 //no dues                                 
		   );
		$obj->Update_data("tbl_agent",$form_data,"where ag_id='$id'");
	}
	// this condition for if delete any given entry then comes bill list
	if($dues>0){
		$form_data=array(                                                
			'pay_status' =>1, //Customer paid                              
		   );
		$obj->Update_data("tbl_agent",$form_data,"where ag_id='$id'");
	}	
}


// ========== Full Due Function Start =================
//This function move the client from bill collection list to dues list
$dltoken = isset($_GET['dltoken'])? $_GET['dltoken']:NULL;
if(!empty($dltoken)){
$form_data=array(
			'due_status' =>1                               
		   );
if($obj->Update_data("tbl_agent",$form_data,"where ag_id='$dltoken'")){
		$details3 = $obj->details_by_cond("tbl_agent","ag_id='$dltoken'");
		extract($details3);
		$notification = 'Full Due Successfully of , '.$details3['ag_name'].", ID: ".$details3['cus_id'];
	}
else{
		$notification = 'Full Due Failed';
	}

}
// ========== Full Due Function End =================


// ========== Full paid Function End =================
if(isset($_GET['token']) && isset($_GET['amount'])){
	   $token=$_GET['token'];
       $form_data = array(
          'agent_id' => $token,
          'acc_amount' =>$_GET['amount'],
          'acc_type' => '3',
          'acc_description' =>"Bill collection full payment",
          'entry_by' => $userid,       
          'entry_date' => $date_time,
          'update_by' => $userid
           );
       $service_add=$obj->Reg_user_cond("tbl_account", $form_data, " ");
       if($service_add){                    
           		$form_data3=array(                                 
					'pay_status' =>0                                 
				   );
				$due_list=$obj->Update_data("tbl_agent",$form_data3,"where ag_id='$token'");
				$details3 = $obj->details_by_cond("tbl_agent","ag_id='$token'");
				extract($details3);
				$notification = 'Full Paid Successfully of , '.$details3['ag_name'].", ID: ".$details3['cus_id'];
						?>
					<script>
						alert("<?=$notification?>");
						window.location="?q=view_due_payment";
					</script> 
					<?php
       }else{
           $notification = 'Full Paid Failed! try again';
       }
   }
// ========== Full Due Function End =================




?>


 <div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
        <h3><?php echo isset($notification)? $notification :NULL; ?></h3>
 </div>
<div class="col-md-12" style=" background:#606060; margin-top:20px; margin-bottom: 15px; min-height:45px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">
    <div class="col-md-6">
         <b>View Customer Information</b>
    </div>           
    <div class="col-md-6" style="">
       <?php if($ty=='SA'){ ?>
        <a class="addbutton" href="?q=add_agent">ADD NEW<span class="glyphicon glyphicon-plus"></span></a>
        <?php } ?> 
    </div>
</div>
<div class="row">
	<div class="col-md-1 col-md-offset-9" >
		<a href="?q=view_report_paganition&flag=INVOICE" target="_blank" class="btn btn-primary pull-right"  >Print Invoice</a>
	</div>
	<div class="col-md-1 pull-right" >
		<a href="?q=view_report_paganition&flag=RECEIPT" target="_blank" class="btn btn-primary pull-right"  >Print Receipt</a>
	</div>
</div>
<?php
$msg =isset($_GET['msg']) ? $_GET['msg']:null;
if($msg !=null){
?>
<div class="col-md-12">
  <div class="alert alert-info">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Message: </strong><?=$msg?>
  </div>
</div>
<?php
}
?>
<div class="row" style="font-size: 10px;">         
    <div class="col-md-12 table-responsive">
            <table class="table table-responsive table-bordered table-hover table-striped" id="example">
                <thead> 
                    <tr>
                        <th>Customer Name</th>
                        <th>Customer ID</th>
                        <th>Address</th>
                        <th>Mobile No</th>                       
                        <th>Speed Amount</th>                        
                        <th>Amount of Dues</th>                        
                        <th>Email</th>
                        <th>Billing Category</th>                     
                        <th class="d">Action</th>
                    </tr>
                </thead>
				<tbody>
                   <?php
                  
                    $i='0';
                    foreach ($obj->view_all_by_cond("tbl_agent","ag_status='1' and pay_status='1' AND due_status='0' ") as $value){
                        $i++;                                                              
                    ?>
                    <tr>
                    <td><?php echo isset($value['ag_name'])?$value['ag_name']:NULL;?></td>
                    <td>
                        <a href="?q=view_customer_payment_individual&token2=<?php echo isset ($value['ag_id'])?$value['ag_id']:NULL;?>" ><?php echo isset($value['cus_id'])?$value['cus_id']:NULL;?></a>
                    </td>
                    <td><?php echo isset($value['ag_office_address'])?$value['ag_office_address']:NULL;?></td>
                    <td><?php echo isset($value['ag_mobile_no'])?$value['ag_mobile_no']:NULL;?></td>
                    <td><?php echo isset($value['mb'])?$value['mb']:NULL;?></td>
                    <td><?php echo get_customer_dues(isset($value['ag_id'])?$value['ag_id']:NULL);?></td>
                    <td><?php echo isset($value['ag_email'])?$value['ag_email']:NULL;?></td>
                    <td>
                        <?php 
                        if($value['bill_cat']=='1'){echo 'Monthly';}
                        else if($value['bill_cat']=='2'){echo 'Half Yearly';}
                        else if($value['bill_cat']=='3'){echo 'Yearly';}
                        
                        ?>
                    </td>                                          
                    <td class="d">
                        <div class="btn-group">                            
                            <a href="?q=view_due_payment&token=<?php echo isset($value['ag_id'])? $value['ag_id'] :NULL; ?>&amount=<?php echo isset($value['taka'])?$value['taka']:NULL;?>&flag=1" class="btn btn-info" style="padding:5px">Paid </a>
                            <a href="?q=view_due_payment&dltoken=<?php echo isset($value['ag_id'])? $value['ag_id'] :NULL; ?>" class="btn  btn-danger" style="padding:5px">Due </a>
                            <a href="?q=add_payment&token1=<?php echo isset($value['ag_id'])? $value['ag_id'] :NULL; ?>" style="padding:5px" class="btn btn-success" name="submit"> Edit </a>
                        </div>                              
                    </td>
                    </tr>
                    <?php
                    }                    
                    ?>    
				</tbody>
                </table>
    </div>
</div>
<style>
.dataTables_wrapper{
	top:10px;
	left:-15px;
	width: 103%;
	
}
.d{
	width: 15%;
}
.d a{

}
#example_filter{
	position:absolute;
	right:25px;
}
</style>
<script>
function printDiv(divName) {
	$(".print").css("margin-left","0px");
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>