<?php
include '../model/oop.php';
$obj = new Controller();
$custom_details="";
$i=0;
$type="Receipt";
$token = isset($_GET['token'])? $_GET['token'] :NULL;

$html="";
$account = $obj->details_by_cond('tbl_account',"acc_id='$token'");
$amount=$account['acc_amount'];
$acc_description=$account['acc_description'];
$entry_date=date("d.m.Y", strtotime($account['entry_date']));
$date =date('d.m.Y');
$month =date('F',strtotime($account['entry_date']));
$prefix =date('Ym',strtotime($account['entry_date']));
$id=$account['agent_id'];
$detailsid = $obj->details_by_cond('tbl_agent',"ag_id='$id'");
//=====================start==============================

function convert_number_to_words($number) {
   
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
   
    if (!is_numeric($number)) {
        return false;
    }
   
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
   
    $string = $fraction = null;
   
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
   
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
   
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
   
    return $string;
}

//=======================end============================
$i=1;
$sum=0;
$mainbody="";
$diff=0;
$tr='';
$custom_details ='<tr><td style="color:#E9EAEC;">0</td><td ></td><td></td><td></td><td></td></tr>';
	$tr='';
	$tr = $custom_details.$custom_details;
	
	$mainbody .='
	<div width"100%" style="height=""297mm">
	<div style="width:100%;height:30px;">
		<img src="./img/header.png" width="100%"/>
	</div>
	<div width"100%" style="padding-left:12%;font-size:12px;margin-top:-75px;">
<p>To</p>
<p  style="font-size:16px;"><b>'.$detailsid['ag_name'].'</b></p>
'.$detailsid['ag_office_address'].'
</div>
<div style="width:100%;display:inline-block;margin-top:25px;color:white;font-size:14px;">
<div width="27%" height="40px" style="text-align:center;font-size:32px;font-weight:900;background-color:#575757;display:inline-block;padding:25px;float:left;border-right:3px solid white;" >
'.$type.'
</div>
<div width="60%" height="42px" style="background-color:#575757;display:inline-block;padding:25px;float:left;text-align:center;" >
<div width="33.33%" height="42px" style="background-color:#575757;display:inline-block;float:left;" >
Total Due:<br>'.number_format($amount,2,".",",").' BDT
</div>
<div width="33.33%" height="42px" style="background-color:#575757;display:inline-block;float:left;" >
'.$type.' No.<br>'.$prefix.$token.'
</div>
<div width="33.33%" height="42px" style="background-color:#575757;display:inline-block;float:left;" >
Date:<br>
'.$entry_date.'
</div>
</div>
</div>
<br />
<br />

<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<thead>
	<tr>
		<td width="7%">Sl.</td>
		<td width="48%">Description</td>
		<td width="10%">Month</td>
		<td width="10%">Speed</td>
		<td width="25%">Bill Amount</td>
	</tr>
	
</thead>
<tbody>
<!-- ITEMS HERE start here value comes from top -->
	<tr>
		<td align="center">1</td>
		<td align="left">'.$acc_description.'</td>
		<td align="center">'.$month.'</td>
		<td align="center">'.$detailsid['mb'].'</td>
		<td align="center">'.number_format($amount,2,".",",").'</td>
	</tr>
	'.$tr.'
<!-- END ITEMS HERE -->
</tbody>
</table>
<div style="width:100%;display:inline-block;margin-top:100px;margin-bottom:50mm;color:white;font-size:14px;">
<div width="60%" height="40px" style="text-align:center;font-size:32px;font-weight:900;display:inline-block;padding:5px;float:left;border-right:3px solid white;" ></div>
<div width="38%" style="display:inline-block;float:left;text-align:center;" >
<div  style="background-color:#575757;display:inline-block;float:left;padding:5px;" >
Total : '.number_format($amount,2,".",",").' BDT
</div>
<div  style="text-align:left;display:inline-block;float:left;color:black;padding-top:10px;" >
In word:<br>
'.convert_number_to_words($amount).'
</div>

</div>
</div>
</div>
	';


$html .= '
<html>
<head>
<style>
body {font-family: sans-serif;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.1mm solid #000000;
}
tbody td { 
vertical-align: top;
background-color: #E9EAEC;
 }
.items td {
	border: 1.4mm solid #FFFFFF;
	vertical-align:middle;
	font-size:14px;
	padding:15px;
	
}
table thead td { 
	text-align: center;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">

</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
Page {PAGENO} of {nb}
</div>
<div style="width:100%;height:30px;">
	<img src="./img/foot2.png" width="100%"/>
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
'.$mainbody.'
</body>
</html>
';
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================

define('_MPDF_PATH','./');
include("./mpdf.php");

$mpdf=new mPDF('c','A4','','',0,0,0,0,0,0); 
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle($type." | Developed By BSD");
$mpdf->SetAuthor("MD Furkanul Islam.");
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);


$mpdf->Output(); exit;

exit;

?>