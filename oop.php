<?php
class Controller{

//Start DB Connection for Online

    // private $host="localhost";
    // private $user="globew5_office";
    // private $db="globew5_office_db";
    // private $pass="OFFICE_123";

//End DB Connection for Online
    
    
//Start DB Connection for Local
    
    private $host="localhost";
    private $user="bsdbd_isp";
    private $db="bsdbd_isp";
    private $pass="isp123";
    
    public function __construct(){
        $this->con = new PDO("mysql:host=".$this->host.";dbname=".$this->db,$this->user,$this->pass);
        
    }
    
//End DB Connection for Local



public function login_check($table_name,$where_cond) {
    $sql_login = "SELECT * FROM ".$table_name." WHERE $where_cond";
    $login = $this->con->prepare($sql_login);
    $login->execute();
    $total = $login->rowCount();

    if($total==1){

    $data = $login->fetch(PDO::FETCH_ASSOC);
    return isset($data)? $data :NULL;

    }

    else{
        return $total;
    }
    
}


// Career Registration Function

public function Reg_user_cond($table_name, $form_data,$where_cond) {
    $fields = array_keys($form_data);

    $sql_login = "SELECT * FROM ".$table_name." WHERE $where_cond";
    $login = $this->con->prepare($sql_login);
    $login->execute(array());
    $total = $login->rowCount();

    if($total=='0'){

    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";
    $q = $this->con->prepare($sql);
    $q->execute() or die(print_r($q->errorInfo()));

    return $this->con->lastInsertId();
    }
}



  // Data Insert Function
    public function Insert_data($table_name, $form_data) {
    $fields = array_keys($form_data);

     $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";
   
    $q = $this->con->prepare($sql);
    $q->execute() or die(print_r($q->errorInfo()));

    return $this->con->lastInsertId();
}



// View All Data Function
public function view_all($table_name) {
    $data = array();
    $sql = "SELECT * FROM $table_name";
    $q = $this->con->prepare($sql);
    $q->execute();
    
    while ($row = $q->fetch(PDO::FETCH_ASSOC)){
        $data[] = $row;
    }
    return $data;    
}
// View All Data Function
public function get_all_expense($dateform,$dateto) {
    $data = array();
    $sql = "SELECT (SELECT acc_name FROM tbl_accounts_head Where acc_id=acc_head ) as name, acc_head,SUM(acc_amount) as payments FROM tbl_account WHERE acc_type='1' AND MONTH(entry_date)='$dateform' AND YEAR(entry_date)='$dateto' GROUP BY acc_head ORDER BY entry_date ASC";
    $q = $this->con->prepare($sql);
    $q->execute();
    while ($row = $q->fetch(PDO::FETCH_ASSOC)){
        $data[] = $row;
    }
    return $data;    
}
//Sum of Expense
public function get_sum_expense($dateform,$dateto) {
    $data = array();
    $sql = "SELECT SUM(acc_amount) as amount FROM tbl_account WHERE acc_type='1' AND MONTH(entry_date)='$dateform' AND YEAR(entry_date)='$dateto'";
    $q = $this->con->prepare($sql);
    $q->execute();
    $data = $q->fetch(PDO::FETCH_ASSOC);
    return $data;   
}
// sum of all income
public function get_all_income($dateform,$dateto) {
    $data = array();
    $sql = "SELECT SUM(amount) as amount FROM vw_all_income WHERE MONTH(entry_date)='$dateform' AND YEAR(entry_date)='$dateto'";
    $q = $this->con->prepare($sql);
    $q->execute();
    $data = $q->fetch(PDO::FETCH_ASSOC);
    return $data;    
}
// View All Data Function
public function ex_row($dateform,$dateto) {
    $data = array();
    $sql = "SELECT (SELECT acc_name FROM tbl_accounts_head Where acc_id=acc_head ) as name, acc_head,SUM(acc_amount) as payments FROM tbl_account WHERE acc_type='1' AND MONTH(entry_date)='$dateform' AND YEAR(entry_date)='$dateto' GROUP BY acc_head ORDER BY entry_date ASC";
    $q = $this->con->prepare($sql);
    $q->execute();
    return $q->rowCount();    
}


// View All Ordered By  Data Function
public function view_all_ordered_by($table_name,$order) {
    $data = array();
    $sql = "SELECT * FROM $table_name ORDER BY $order";
    $q = $this->con->prepare($sql);
    $q->execute();
    
    while ($row = $q->fetch(PDO::FETCH_ASSOC)){
        $data[] = $row;
    }
    return $data;    
}



// View All Data Condition wise Function
public function view_all_by_cond($table_name,$where_cond) {
    $data = array();
    $sql = "SELECT * FROM $table_name WHERE $where_cond";
    $q = $this->con->prepare($sql);
    $q->execute();
    
    while ($row = $q->fetch(PDO::FETCH_ASSOC)){
        $data[] = $row;
    }
    return $data;    
}



// Details Data View Condition Wise Function
public function details_by_cond($table_name,$where_cond){

 $sql="SELECT * FROM $table_name WHERE $where_cond";
 $q = $this->con->prepare($sql);
 $q->execute() or die(print_r($q->errorInfo()));
 $data = $q->fetch(PDO::FETCH_ASSOC);
 return $data;
 }


 
// Update Data Function

function Update_data($table_name, $form_data, $where_clause='') {

    $whereSQL = '';
    if(!empty($where_clause))
    {

        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE')
        {

            $whereSQL = " WHERE ".$where_clause;
        } else
        {
            $whereSQL = " ".trim($where_clause);
        }
    }

    $sql = "UPDATE ".$table_name." SET ";

    $sets = array();
    foreach($form_data as $column => $value)
    {
         $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);

    $sql .= $whereSQL;

    $q = $this->con->prepare($sql);
 
 return $q->execute() or die(print_r($q->errorInfo()));
}



// Delete Data Function
function Delete_data($table_name,$where_cond) {

    $sql = "delete FROM $table_name WHERE $where_cond";   
    $q = $this->con->prepare($sql);
    $q->execute() or die(print_r($q->errorInfo()));
    $data = $q->fetch(PDO::FETCH_ASSOC);
    return $data;  
      
}


// Mail Send with Attach file
function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
    $file = $path.$filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    $name = basename($file);
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$replyto."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $header .= $message."\r\n\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
    $header .= "Content-Transfer-Encoding: base64\r\n";
    $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
    $header .= $content."\r\n\r\n";
    $header .= "--".$uid."--";
    if (mail($mailto, $subject, "", $header)) {
        echo "mail send ... OK"; // or use booleans here
    } else {
        echo "mail send ... ERROR!";
    }
}


// Mail send without attach file

function mail_send($to,$subject,$message,$from) {
    
    if (mail($to,$subject,$message,"From: $from\n")) {
        //echo "mail send ... OK"; // or use booleans here
    } else {
        echo "mail send ... ERROR!";
    }
}

// End Mailing Function

public function Total_Count($table_name,$where_cond) {
    $sql_login = "SELECT * FROM ".$table_name." WHERE $where_cond";
    $login = $this->con->prepare($sql_login);
    $login->execute();
    $total = $login->rowCount();

    return $total;

}



//Start Randome Code

function Random_Code($chars,$length) { 

    srand((double)microtime()*1000000); 
    $i = 0; 
    $code = '' ; 

    while ($i <= ($length-1)) { 
        $num = rand() % 33; 
        $tmp = substr($chars, $num, 1); 
        $code = $code . $tmp; 
        $i++; 
        
    
    } 
    return $code;
} 




//End Randome Code

}
?>