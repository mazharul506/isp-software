<?php
date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
$date =date('Y-m-d');
//$date        = date('Y-m-d');
$ip_add = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;

$token = isset($_GET['token1'])? $_GET['token1']:NULL;
// ========== Delete Function Start =================
$dltoken = isset($_GET['dltoken'])? $_GET['dltoken']:NULL;
if(!empty($dltoken)){
$dele = $obj->Delete_data("tbl_account","acc_id='$dltoken'");
if(!$dele)
    {$notification = 'Delete Successfull';}
else
    {$notification = 'Delete Failed';} 
}
//====================Add Funcyion=========================
if(isset ($_POST['submit'])){
       extract($_POST);
       $form_data = array(
           
          'agent_id' => $token,
          'acc_amount' => $amount,
          'acc_type' => '3',
          'acc_description' =>!empty($description)?$description:"Bill collection",
          'entry_by' => $userid,       
          'entry_date' => $date_time,
          'update_by' => $userid
           );
		   // move to due list
		   if(isset($_POST['check'])){
			   $form_data3=array(                                 
					'due_status' =>1                                 
				   );
				$due_list=$obj->Update_data("tbl_agent",$form_data3,"where ag_id='$token'");
		   }
		  //bonus
       $service_add=$obj->Reg_user_cond("tbl_account", $form_data, " ");
	   if(isset($_POST['bonus']) && !empty($_POST['bonus'])){
	   	$form_data2 = array(
		   
          'customerID' => $token,
          'amount' => $bonus,        
          'takenBy' => $userid
           );
       $service_add2=$obj->Reg_user_cond("bonus", $form_data2, " ");
       }  
       if($service_add){                    
           ?>
            <script>               
              window.location="?q=view_due_payment";
            </script>   
<?php                    
       }else{
           echo $notification = 'Insert Failed';
       }
   }
// ========== Delete Function End =================
//------------------------------------------------------
        
			$details = $obj->details_by_cond("tbl_agent","ag_id='$token'");
            extract($details);

            $bday1=$details['entry_date'];


            $bday = new DateTime($bday1);
            $today = new DateTime(date('Y-m-d', time())); // for testing purposes
            $diff = $today->diff($bday);
//          printf('%d years, %d month, %d days', $diff->y, $diff->m, $diff->d);
            
        
        $total=0;      
        $serviceamount1=0;
       
       
         foreach ($obj->view_all_by_cond("tbl_agent","ag_id='$token'") as $details1){
            extract($details1);
            $serviceamount1+=($details1['taka']);
            
            }
            if($diff->m!=0){
            $serviceamount2=($serviceamount1*$diff->y*12)+($serviceamount1*$diff->m);
            }
            else{
               $serviceamount2=$serviceamount1;
            }
            foreach ($obj->view_all_by_cond("vw_account","agent_id='$token' order by acc_id") as $customer_info){
            extract($customer_info);
            $total+=$customer_info['acc_amount'];
            }       
			$bonus=0;
			foreach ($obj->view_all_by_cond("bonus","customerID='$token' ") as $customer_bonus){
            extract($customer_bonus);
            $bonus+=$customer_bonus['amount'];
            }       
       $dueamount=$serviceamount2-($total+$bonus);
	   //pay_status=0 means bill paid
			if($dueamount>=0){
				$form_data=array(                                                
					'pay_status' =>1                                  
				   );
				$obj->Update_data("tbl_agent",$form_data,"where ag_id='$token'");
			}  
//      -----------------------------------------------------
//      







//=====================start==============================

function convert_number_to_words($number) {
   
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
   
    if (!is_numeric($number)) {
        return false;
    }
   
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
   
    $string = $fraction = null;
   
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
   
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
   
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
   
    return $string;
}

//=======================end============================

?>

<script>
    
function numbersOnly(e) // Numeric Validation 
{
    var unicode=e.charCode? e.charCode : e.keyCode
    if (unicode!=8)
    {
        if ((unicode<2534||unicode>2543)&&(unicode<48||unicode>57))
        {
            return false;                       
        }
    }
}

</script>
            
 <div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
        <b><?php echo isset($notification)? $notification :NULL; ?></b>
 </div>
<div class="col-md-12" style=" background:#606060; margin-top:20px; margin-bottom: 15px; min-height:45px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">
    <div class="col-md-6">
         <b>View Customer Payment Information</b>
    </div>               
</div>
<div class="row" style="padding:10px; font-size: 12px;" >   
    <div class="col-md-4">
       <div class="form-group">
            <label>Service Amount</label>
            <input value="<?php echo $serviceamount1 ?>" type="text" name="amount" class="form-control" id="ResponsiveTitle"  readonly="" >
       </div>  
    </div>
    <div class="col-md-4">        
       <div class="form-group">
            <label>Pay Amount</label>
            <input value="<?PHP echo $total; ?>" type="text" name="amount" class="form-control" id="ResponsiveTitle" readonly="" >
       </div>  
    </div>
    <div class="col-md-4">
       <div class="form-group">
            <label>Due Amount</label>
            <input value="<?php echo $dueamount ?>" type="text" name="amount" class="form-control" id="ResponsiveTitle"  readonly="" >
       </div>  
    </div>
</div>
                   
<form id="paid_form" role="form" enctype="multipart/form-data" method="post" action=" ">    
	<div class="row" style="font-size: 12px;">

		<div class="col-md-2">
		   <div class="form-group">
				 <label  >Payment Date</label>
				 <input value="<?php echo $date ?>"  type="text"   id="ResponsiveTitle" readonly=""  >
		   </div>
			
			<div class="form-group" style="display: none;">
				 <label class="col-sm-6" >Customer Name</label>
				 <input value="<?php echo $customername ?>" type="text" name="customer_id" class="form-control" id="ResponsiveTitle" readonly=""  >
		   </div> 
		</div>
		<div class="col-md-3 col-md-offset-1">
		   <div class="form-group">
			   <label >Payment Amount</label>
			   <input id="in_amount" onkeypress="return numbersOnly(event)" style="height: 30px;"  class="col-sm-7" type="text" name="amount"  required="">
		   </div>  
		</div>
		<div class="col-md-2">
		   <div class="form-group">
			   <label >Discount(optional)</label>
			   <input class="col-sm-12" placeholder="Bonus(optional)" onkeypress="return numbersOnly(event)" style="height: 30px;"  type="text" name="bonus">
		   </div>  
		</div>
		<div class="col-md-2 text-center">
		   <div class="form-group">
			   <label >Move to due list(optional)</label>
			   <input class="col-sm-12"  style="height: 30px;" type="checkbox" value="Move to due list" name="check">
		   </div>  
		</div>
		
	</div>
	<div class="row" style="font-size: 12px;">

		<div class="col-md-3 col-md-offset-3">
		   <div class="form-group">
				 <label  >Payment Description</label>
				 <input type="text" style="height: 30px;" id="in_des" name="description" placeholder="Enter Description...">
		   </div>
		</div>
		<div class="col-md-2">
			<button id="btn_submit" style="padding: 10px !important;margin-top:10%;" type="submit" class="btn btn-lg btn-success" name="submit">Add Payment</button>                  
		</div>
	</div>
</form>

<div class="row" style="padding:10px; font-size: 12px;">         
    <div class="col-md-12">       
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped" id="example">
                <thead> 
                    <tr>
                        <th>#</th>
                        <th>Date</th>                                           
                        <th>Description</th>                      
                        <th>Amount</th>                                               
                        <th>Received By</th>                                             
                        <th>Action</th>
                    </tr>
                </thead>                   
                        <?php
                        $i=0;
                        $totalin=0;
                        foreach ($obj->view_all_by_cond("vw_account","agent_id='$token' order by acc_id") as $customer_info){
                        extract($customer_info);
                        $i++;
                        $totalin+=$customer_info['acc_amount'];
                        ?>
                        <tr>
                            <td><?php echo $i;  ?></td>
                            <td><?php echo date("d-m-Y", strtotime(isset($customer_info['entry_date'])?$customer_info['entry_date']:"2016-02-1")); ?></td>
                            <td style="text-align: right;"><?php echo isset($customer_info['acc_description'])?$customer_info['acc_description']:NULL; ?></td>
                            <td style="text-align: right;"><?php echo isset($customer_info['acc_amount'])?$customer_info['acc_amount']:NULL; ?></td>
                            <td style="text-align: right;"><?php echo isset($customer_info['FullName'])?$customer_info['FullName']:NULL; ?>(<?php echo isset($customer_info['UserName'])?$customer_info['UserName']:NULL; ?>)</td>
                              
                            <td>                          
                                <div class="btn-group" > 
                                    <?php foreach ($acc as $per){if($per=='edit'){ ?>                                                                           
                                    <a class="btn btn-xs btn-info" style="margin-top: 2px;" href="?q=edit_agent_payment&token=<?php echo isset ($customer_info['acc_id'])?$customer_info['acc_id']:NULL?>">
                                       <span class="glyphicon glyphicon-edit"></span>
                                    </a> 
                                    <?php 
                                    }} 
                                    foreach ($acc as $per){if($per=='delete'){
                                    ?>                             
                                    <a href="?q=add_payment&token1=<?=$token?>&dltoken=<?php echo isset($customer_info['acc_id'])? $customer_info['acc_id'] :NULL; ?>" class="btn btn-xs btn-danger" style="margin-left: 5px;">
                                       <span class="glyphicon glyphicon-remove"></span>
                                    </a> 
                                    <?php }} ?>
                                    <a target="_blank" href="print/payment_invoice.php?token=<?php echo isset ($customer_info['acc_id'])?$customer_info['acc_id']:NULL?>" class="btn btn-xs " style="margin-left: 5px; background: green;">
                                        <span style="color: black;" class="glyphicon glyphicon-print"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php 
                         }
                        ?>
                      <tr >                          
                          <td colspan="2" style="text-align: right;">Total:</td>
                          <td style="text-align: right;"><?PHP echo $totalin; ?></td>                                   
                      </tr>    
                      <tr>
                          <?php
                            $word=convert_number_to_words($totalin);
                          ?>                          
                          <td colspan="5" style="text-align: center;" ><span style="color: green;">Total Ammount In Word::&nbsp;&nbsp;&nbsp;<span style="color: red;"><?PHP echo $word; ?></span></span></td>                          
                      </tr>    

                
                </table>
            </div>
    </div>
</div>
<?php
//check for full payment or not
$flag = isset($_GET['flag'])? $_GET['flag']:"0";
if($flag=="1"){
	$auto_amount=isset($_GET['amount'])? $_GET['amount']:"0";
?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$("#in_amount").val("<?=$auto_amount?>");
	$("#in_des").val("Bill Collection Full payment");
	$('#btn_submit').click();
});
</script>
<?php
}
?>