<?php

date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
     
//$date        = date('Y-m-d');
$ip_add      = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;

// ========== Delete Function Start =================
$dltoken = isset($_GET['dltoken'])? $_GET['dltoken']:NULL;
if(!empty($dltoken)){

$dele = $obj->Delete_data("header_tbl","header_id='$dltoken'");

if(!$dele)
    {$notification = 'Delete Successfull';}
else
    {$notification = 'Delete Failed';} 
}
// ========== Delete Function End =================



if(isset($_POST['submit'])){

//Start Random code Generator
$seed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
$rand = array_rand($seed, 6);
$convert = array_map(function($n){
    global $seed;
    return $seed[$n];
},$rand);
$character = implode('',$convert);

$seed = str_split('1234567890');
$rand = array_rand($seed, 4);
$convert = array_map(function($n){
    global $seed;
    return $seed[$n];
},$rand);
$digit = implode('',$convert);

$rend_code = "BSTL" . "$character" . "$digit";

//End Random code Generator


$nep = "asset/logo/" . $rend_code . ".jpg";
if ($_FILES["LogoPath"]["name"])

if (copy($_FILES["LogoPath"]["tmp_name"], $nep)) 
$photo_path    = "asset/logo/" . $rend_code . ".jpg"; 
$user_photo_path = isset($photo_path) ? $photo_path :NULL; {

        extract($_POST);
        $form_data = array(
          'lang_id' => $LanguageId,
          'org_name' => $CompanyName,
          'org_motto' => $CompanyMotto,
          'search_name' => $Search_box,
          'sign_in' => $sign_in,
          'sign_out' => $sign_out,
          'photo_path' => $photo_path,
          'entry_by' => $userid,       
          'entry_date' => $date_time,
          'update_by' => $userid
          );
        $created_id = $obj->Reg_user_cond("header_tbl", $form_data, "lang_id='$LanguageId'");

            if($created_id)
              {$notification = 'Saved Successfull';}
              else
              {$notification = 'Already Exist or Saved Failed';}            
          
      }

   }

?>


        <div class="col-md-12" style=" background-image:url(asset/img/content_h1.png); margin-top:20px; margin-bottom: 15px; min-height:40px; padding:8px 0px 0px 15px; font-size:16px; font-family:Lucida Sans Unicode; color:#FFFFFF; font-weight:bold;">
            <b>Header Information Entry From </b>
        </div>
       <div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
            <b><?php echo isset($notification)? $notification :NULL; ?></b>
        </div>
                
        <div class="row" style="padding:10px; font-size: 12px;">
        <form role="form" enctype="multipart/form-data" method="post">    
            <div class="row" style="padding:10px; font-size: 12px;">

                <div class="col-md-6">


                      <div class="form-group">
                    <label for="exampleInputEmail1">Language Name </label>
                        <select class="form-control" required="required" name="LanguageId" id="LanguageId">
                        <option value="">Select Language - - -</option>
                              <?php
                                  foreach ($obj->view_all("_languages") as $value){
                                  extract($value);
                              ?>
                          <option value="<?php echo isset($value['LanguageId'])? $value['LanguageId']:NULL; ?>"><?php echo isset($value['LanguageName'])? $value['LanguageName']:NULL; ?></option>       
                              <?php } ?>
                      </select>
                  </div>
                    <div class="form-group">
                        <label>Organization Name</label>
                        <input type="text" name="CompanyName" class="form-control" id="CompanyName" placeholder="Organization Name" required>
                     </div>
                    <div class="form-group">
                        <label>Organization Motto</label>
                        <input type="text" name="CompanyMotto" class="form-control" id="CompanyMotto" placeholder="Organization Motto" required>
                     </div>
                     <div class="form-group">
                        <label>Search Box Name</label>
                        <input type="text" name="Search_box" class="form-control" id="Search_box" placeholder="Search Box Name" required>
                     </div>
                     <div class="form-group">
                        <label>Sign In Name</label>
                        <input type="text" name="sign_in" class="form-control" id="sign_in" placeholder="Sign In Name" required>
                     </div>
                     <div class="form-group">
                        <label>Sign Out Name</label>
                        <input type="text" name="sign_out" class="form-control" id="sign_out" placeholder="Sign Out Name" required>
                     </div>
                </div>

                <div class="col-md-2"></div>

                <div class="col-md-4">
                  <div class="form-group">
                    <img width="140" height="140" src="asset/img/def_img.png" alt="..." class="img-thumbnail" id="pre_photo" >
                  </div>
                <div class="form-group">
                    <label for="exampleInputFile">Logo</label>
                    <input type="file" onchange="usershow_photo(this)" id="LogoPath" name="LogoPath" required>
                  </div>

                </div>
            </div>

            <div class="row" style="padding: 5px 0px 15px 25px; font-size: 12px;">
              <button type="submit" name="submit" class="btn btn-success">Submit</button> 
            </div>
        </form>
        </div>

<hr></hr>
<p class="TableDataHint">Header Table view for Edit and Delete</p>
        <div class="row" style="padding:10px; font-size: 12px;">
          
            <div class="col-md-12">

                <table class="table table-condensed">
                    <tr>
                        <th style="width: 20px;">#</th>
                        <th>Language Name</th>
                        <th>Organization Name</th>
                        <th>Organization Motto</th>
                        <th>Search Name</th>
                        <th>Sign In Name</th>
                        <th>Sign Out Name</th>
                        <th>logo</th>
                        <th class="with50">Action</th>
                    </tr>
                    <?php
                    $i = '0';
                    foreach ($obj->view_all("vw_header_tbl") as $value){
                    extract($value);
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo isset($value['LanguageName'])? $value['LanguageName']:NULL; ?></td>
                        <td><?php echo isset($value['org_name'])? $value['org_name']:NULL; ?></td>
                        <td><?php echo isset($value['org_motto'])? $value['org_motto']:NULL; ?></td>
                        <td><?php echo isset($value['search_name'])? $value['search_name']:NULL; ?></td>
                        <td><?php echo isset($value['sign_in'])? $value['sign_in']:NULL; ?></td>
                        <td><?php echo isset($value['sign_out'])? $value['sign_out']:NULL; ?></td>
                        <td><img src="<?php echo isset($value['photo_path'])? $value['photo_path']:NULL; ?>" width="80px" height="80px" ></img></td>
                        <td>

                            <div class="btn-group"> 
                              <?php foreach ($acc as $per){if($per=='edit'){ ?>                                                                            
                                <a href="?q=edit_header&token=<?php echo isset($value['header_id'])? $value['header_id']:NULL; ?>" class="btn btn-xs btn-info">
                                    <span class="margins glyphicon glyphicon-edit"></span>                                   
                                </a>
                                <?php 
                                }} 
                                foreach ($acc as $per){if($per=='delete'){
                                ?>
                                <a href="?q=header&dltoken=<?php echo isset($value['header_id'])? $value['header_id']:NULL; ?>" class="btn btn-xs btn-danger" style="margin-left: 5px;">
                                     <span class=" margins glyphicon glyphicon-remove"></span>
                                </a> 
                                <?php }} ?>                                                                                                                                    
                            </div>   
                        </td>
                        
                    </tr>
                    <?php
                    }
                    ?>

                </table>

                
            </div>

            <div class="col-md-2"></div>
            
            

          
        </div>

       