<?php

date_default_timezone_set('Asia/Dhaka');
$date_time =date('Y-m-d g:i:sA');
//$date        = date('Y-m-d');
$ip_add = $_SERVER['REMOTE_ADDR'];
$userid =isset($_SESSION['UserId']) ? $_SESSION['UserId']:NULL;


$data= $obj->details_by_cond("tbl_agent","ag_status='1' ORDER BY ag_id DESC");

//$id= intval($data['ag_id']);
 
   if(($data['ag_id']+1)<10){
       $STD = "CUS000";
   }
   else if(($data['ag_id']+1)<100){
       $STD = "CUS000";
   }
   else if(($data['ag_id']+1)<1000){
       $STD = "CUS00";
   }
   else if(($data['ag_id']+1)<10000){
       $STD = "CUS0";
   }
   else{
       $STD = "CUS";
   }
   $STD .= $data['ag_id']+1;
   

//===================Add Function===================

   if(isset ($_POST['submit'])){
       extract($_POST);
       
       $form_data = array(
           
          'ag_name' => $Name,         
          'ag_mobile_no' => $mobile,
          'ag_office_address' => str_replace("'", "", $Details),
          'taka' => $taka,
          'mb' => $mb,
          'ag_email' => $email,
          'bill_cat' => $bill_cat,                               
          'cus_id' => $STD,  
          'ag_status' => $status,  
          'connect_charge' => $connect_charge,  
           
          'entry_by' => $userid,       
          'entry_date' => $date_time,
          'update_by' => $userid
           );
       $service_add=$obj->Reg_user_cond("tbl_agent", $form_data, " ");
       
       if($service_add){                      
           ?>
            <script>
              window.location="?q=view_agent";
            </script>   
<?php                    
       }
       else{
           echo $notification = 'Insert Failed';
       }
   }
?>
<!--===================end Function===================-->
<script>
    
function numbersOnly(e) // Numeric Validation 
{
    var unicode=e.charCode? e.charCode : e.keyCode
    if (unicode!=8)
    {
        if ((unicode<2534||unicode>2543)&&(unicode<48||unicode>57))
        {
            return false;                       
        }
    }
}

</script>

<div class="col-md-12" style=" margin-top:5px; margin-bottom: 5px; font-size:14px;  color:red; font-weight:bold; text-align: center;">
    <b><?php echo isset($notification)? $notification :NULL; ?></b>
</div>
<div class="row" style="padding:10px; font-size: 12px;">
          <form role="form" enctype="multipart/form-data" method="post">    
                <div class="row" style="padding:10px; font-size: 12px;">

                    <div class="col-md-6">
                        <span style="color: red; font-size: 20px;"><?php echo $STD; ?></span>
                       <div class="form-group">
                            <label>Customer Name</label>
                            <input type="text" name="Name" class="form-control" id="ResponsiveTitle" required="required" >
                       </div>                                            
                       <div class="form-group">
                            <label>Mobile No</label>
                            <input type="text" name="mobile" class="form-control" id="ResponsiveTitle" required="required" >
                       </div>
                       <div class="form-group">
                            <label>Address</label>
                             <textarea class="form-control" name="Details" id="ResponsiveDetelis" rows="6"></textarea>
                        </div>
                                            
                        <div class="form-group">
                            <label>Amount Of Taka</label>
                            <input onkeypress="return numbersOnly(event)" type="text" name="taka" class="form-control" id="ResponsiveTitle"  >
                       </div>                        
                        <div class="form-group">
                            <label>Connect Charge</label>
                            <input onkeypress="return numbersOnly(event)" type="text" name="connect_charge" class="form-control" id="ResponsiveTitle"  >
                       </div>                        
                        <div class="form-group">
                            <label>Amount Of MB</label>
                            <input type="text" name="mb" class="form-control" id="ResponsiveTitle"  >
                       </div>                        
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" id="ResponsiveTitle" required="required" >
                       </div> 
                                                                                                         
                        <div class="form-group">                                                         
                            <label>Bill Category</label>
                            <select class="form-control" required="required" name="bill_cat" id="status">
                                <option value="">---Category Select---</option>
                                <option value="1">Monthly</option>
                                <option value="2">Half Yearly</option>
                                <option value="3">Yearly</option>
                            </select>                       
                         </div>                       
                        <div class="form-group">                                                         
                            <label>Status</label>
                            <select class="form-control" required="required" name="status" id="status">
                                <option value="">---Status---</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>                       
                        </div>
                        <div class="row" style="text-align: center; padding: 5px 0px 15px 25px; font-size: 12px;">
                            <button type="submit" class="btn btn-success" name="submit">Submit</button> 
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>

             
        </form>
        </div>
<hr>